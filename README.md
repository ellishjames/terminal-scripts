# Terminal Scripts

A public repository of bash scripts or batch scripts I've written and use on personal devices, work devices, and servers on a daily basis.

You may ask that why don't I use alises for some of these? I use fish as my terminal instead of bash so I found this easier to do.
