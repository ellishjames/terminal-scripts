# clockify-cli bash layer

These two bash scripts are meant to be used with `clockify-cli`, you must customise them to your projects. To get the IDs and Names of your Projects on Clockify, you need to set up `clockify-cli` first then customise the script to fit you.

Download `clockify-cli` from [here](https://github.com/lucassabreu/clockify-cli/releases).

The Darwin relase is for MacOS.

Extract `clockify-cli` to your `/usr/local/bin` directory.

Sign in or sign up for a Clockify Account, go to user settings, found [here](https://clockify.me/user/settings). Scroll to the bottom and grab your API key.

Run `clockify-cli config init` in your terminal and use your API key when it asks for your `User Generated Token`. `clockify-cli` makes a config file in your `home` folder named `.clockify-cli.yaml`.

To get a list of projects on Clockify using the CLI, run `clockify-cli project list`. The IDs and Names in this list is what you'll use to customise `clock-start` and `clock-stop`.
