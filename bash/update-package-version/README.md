# update-package-version

Updates the package.json version of a repository.

I use this script in most of repositories since I like the idea of date versioning model.

## Versioning Model

YY.MD.PATCH

YEAR.MONTHDAY.PATCH

- We increase the PATCH version on release branches when we need to do a hotfix.
- On the develop branch the patch version will remain at 0.
- A pipeline wherever will run and try to update the package.json version every day by increasing the year, month, and day.
