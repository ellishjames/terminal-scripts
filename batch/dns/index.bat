netsh interface ipv4 set dns "Ethernet" static 1.1.1.1
netsh interface ipv4 add dns "Ethernet" 1.0.0.1 index=2
netsh interface ipv6 set dns "Ethernet" static 2606:4700:4700::1111
netsh interface ipv6 add dns "Ethernet" 2606:4700:4700::1001 index=2
netsh interface ipv4 set dns "WiFi" static 1.1.1.1
netsh interface ipv4 add dns "WiFi" 1.0.0.1 index=2
netsh interface ipv6 set dns "WiFi" static 2606:4700:4700::1111
netsh interface ipv6 add dns "WiFi" 2606:4700:4700::1001 index=2