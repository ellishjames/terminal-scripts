# DNS Batch Script

This sets my DNS settings for my Network Adpaters on Windows. I use a VPN every once and a while, and when I connect to it it'll change the DNS settings on those adapters. But when I disconnect it won't change back to my preferred settings, so I use this script to do that instead of clicking through a bunch of dialogs.

This script needs to be run as administrator. 
